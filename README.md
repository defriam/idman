# IDMan (SA ID Validator)

*Just using one git repo for both API and UI*

---
### Running the API

```
# run unit Tests
dotnet test IdManAPI/IdMan.API.Tests/IdMan.API.Tests.csproj 

# run API
# On startup the API will seed dummy data into the Sqlite database
# Seed data Data/Seeder/SampleData/
# default password for all users is 'password'

cd ./IdManAPI/IdMan.API
dotnet watch run

```  

### Running the Web Client

```
cd ./IdMan-SPA

# Requires node 10.x
npm install -g @angular/cli
npm install

# Run unit tests
ng test

# Host UI
ng serve

```

### Setting up API

```
dotnet new sln -o IdMan.sln

# create new nunit test project 
dotnet new nunit --name IdMan.API.Tests

# adding reference
dotnet add IdMan.API.Tests/IdMan.API.Tests.csproj reference IdMan.API/IdMan.API.csproj

# add project to sln 
dotnet sln IdMan.API.sln add IdMan.API/IdMan.API.csproj
dotnet sln IdMan.sln add IdMan.API.Tests/IdMan.API.Tests.csproj

# build solution via cli
dotnet build IdMan.sln

# run unit test via cli
dotnet test IdMan.API.Tests/IdMan.API.Tests.csproj

# run API
cd ./IdMan.API
dotnet watch run

```

### Installing packages

```
cd ./IdMan.API.Tests 
dotnet add package Moq 

```

### Database Prepare

```
dotnet ef database drop --force  
dotnet ef migrations add InitialCreate  
dotnet ef database update
dotnet watch run  

```

### Setting up UI

```
npm install -g @angular/cli  
cd ./IdMan-SPA  
npm install
ng serve

```

## References
  
[Implementing and Error Interceptor Blog 1](https://scotch.io/bar-talk/error-handling-with-angular-6-tips-and-best-practices192)  
[Implementing and Error Interceptor Blog2](https://medium.com/@sub.metu/angular-http-interceptors-fdda14a57660)  
[Managing App Secrets](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-3.0&tabs=windows)  
[AutoMapper Extensions](https://medium.com/ps-its-huuti/how-to-get-started-with-automapper-and-asp-net-core-2-ecac60ef523f)  
[SAID Number Validation](https://mybroadband.co.za/news/security/303812-what-your-south-african-id-number-means-and-what-it-reveals-about-you.html)   
[Angular jwt](https://github.com/auth0/angular2-jwt)  
[getbootstrap](https://getbootstrap.com/docs/4.3/components/dropdowns/)   
[bootswatch](https://bootswatch.com/)  
[route-guards](https://alligator.io/angular/route-guards/)  
[My Design Document](https://defriaconflunce.atlassian.net/wiki/spaces/DOCS/pages/131073/Software+Design+Document)  
[Generate (Fake) South-African ID Numbers](https://chris927.github.io/generate-sa-idnumbers/#)  


## Tools

[Generate Fake ID Number](https://chris927.github.io/generate-sa-idnumbers/)  
[Sqlite DB Browser](https://sqlitebrowser.org/)  


## Issues

[cannot-find-command-dotnet-ef](https://stackoverflow.com/questions/56862089/cannot-find-command-dotnet-ef)  
