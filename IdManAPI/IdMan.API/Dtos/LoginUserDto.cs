using System.ComponentModel.DataAnnotations;

namespace IdMan.API.Dtos
{
    public class LoginUserDto
    {
        [Required] public string Username { get; set; }
        [Required] public string Password { get; set; }
    }
}