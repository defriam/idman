using System.ComponentModel.DataAnnotations;

namespace IdMan.API.Dtos
{
    public class IdentityDto
    {
        [Required]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "RSA ID must be 13 digits")]
        public string IdNumber { get; set; }

        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Citizenship { get; set; }
    }
}