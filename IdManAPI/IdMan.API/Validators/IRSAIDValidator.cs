namespace IdMan.API.Validators
{
    public interface IRSAIDValidator
    {
        string IdNumber { get; set; }
        string DateOfBirth { get; set; }
        string Gender { get; set; }
        string Citizenship { get; set; }

        bool Validate(string idNumber);
    }
}