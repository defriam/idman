using System.Linq;
using System.Text.RegularExpressions;

namespace IdMan.API.Validators
{
    public class RSAIDValidator : IRSAIDValidator
    {
        public string IdNumber { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Citizenship { get; set; }

        public bool Validate(string idNumber)
        {
            idNumber = Regex.Replace(idNumber, @"\s+", "");
            if (idNumber.Length != 13) return false;

            var digits = new int[13];
            for (var i = 0; i < 13; i++) digits[i] = int.Parse(idNumber.Substring(i, 1));

            var control1 = digits.Where((v, i) => i % 2 == 0 && i < 12).Sum();
            var second = string.Empty;
            digits.Where((v, i) => i % 2 != 0 && i < 12).ToList().ForEach(v => second += v.ToString());
            var string2 = (int.Parse(second) * 2).ToString();
            var control2 = string2.Select((t, i) => int.Parse(string2.Substring(i, 1))).Sum();
            var control = (10 - (control1 + control2) % 10) % 10;
            if (digits[12] != control) return false;
            IdNumber = idNumber;
            DateOfBirth = idNumber.Substring(0, 6);
            Gender = digits[6] < 5 ? "Female" : "Male";
            Citizenship = digits[10] == 0 ? "Citizen" : "Resident";
            return true;
        }
    }
}