﻿using System;
using IdMan.API.Data.Repository.Context;
using IdMan.API.Data.Seeder;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace IdMan.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            using (var scope = host.Services.CreateScope())
            {
                var servies = scope.ServiceProvider;
                try
                {
                    var context = servies.GetRequiredService<IdManContext>();
                    context.Database.Migrate();
                    Seed.SeedIdNumbers(context);
                    Seed.SeedUsers(context);
                }
                catch (Exception ex)
                {
                    var logger = servies.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occured during migration");
                }
            }

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
        }
    }
}