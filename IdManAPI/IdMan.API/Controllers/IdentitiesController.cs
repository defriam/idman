using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using IdMan.API.Data.Repository;
using IdMan.API.Dtos;
using IdMan.API.Models;
using IdMan.API.Validators;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;

namespace IdMan.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class IdentitiesController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public IdentitiesController(IUnitOfWork unitOfWork, IMapper mapper, IRSAIDValidator validator)
        {
            Validator = validator;
            Mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        private IMapper Mapper { get; }
        private IRSAIDValidator Validator { get; }

        [HttpGet]
        public async Task<IActionResult> GetIdentities()
        {
            var identities = await _unitOfWork.Identities.GetIdentitiesAsync();

            var mappedIdentities = Mapper.Map<IEnumerable<IdentityDto>>(identities);

            return Ok(mappedIdentities);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetIdentity(int id)
        {
            var identity = await _unitOfWork.Identities.GetIdentityAsync(id);

            var mappedIdentity = Mapper.Map<IdentityDto>(identity);

            return Ok(mappedIdentity);
        }

        [HttpPut("{idNumber}")]
        public async Task<IActionResult> ValidateIdNumber(string idNumber)
        {
            var isValid = Validator.Validate(idNumber);

            if (isValid)
            {
                var idNumberInStore = await _unitOfWork.Identities.FindAsync(x => x.IdNumber == Validator.IdNumber);
                if (idNumberInStore.Any())
                    return StatusCode(400, $"ID number {idNumber} already validated successfully");
            }

            if (!isValid) return StatusCode(400, $"ID number {idNumber} is not a valid RSA ID");

            var identity = Mapper.Map<Identity>(Validator);

            _unitOfWork.Identities.AddAsync(identity);
            var result = await _unitOfWork.Complete();

            const int successCode = 1;
            if (result == successCode)
                return StatusCode(201);
            return StatusCode(500, $"Server could not save ID number {idNumber}");
        }
    }
}