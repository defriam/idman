namespace IdMan.API.Models
{
    public class Identity
    {
        public int Id { get; set; }
        public string IdNumber { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Citizenship { get; set; }
    }
}