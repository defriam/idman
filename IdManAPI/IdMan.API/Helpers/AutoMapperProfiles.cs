using AutoMapper;
using IdMan.API.Dtos;
using IdMan.API.Models;
using IdMan.API.Validators;

namespace IdMan.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Identity, IdentityDto>();
            CreateMap<IRSAIDValidator, Identity>();
        }
    }
}