using System;
using System.Threading.Tasks;
using IdMan.API.Data.Repository.Identity;
using IdMan.API.Data.Repository.User;

namespace IdMan.API.Data.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IIdentityRepository Identities { get; }
        IUserRepository Users { get; }
        Task<int> Complete();
    }
}