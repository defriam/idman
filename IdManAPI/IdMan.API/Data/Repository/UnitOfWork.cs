using System.Threading.Tasks;
using IdMan.API.Data.Repository.Context;
using IdMan.API.Data.Repository.Identity;
using IdMan.API.Data.Repository.User;

namespace IdMan.API.Data.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IdManContext _context;

        public UnitOfWork(IdManContext context)
        {
            _context = context;
            Identities = new IdentityRepository(_context);
            Users = new UserRepository(_context);
        }

        public IIdentityRepository Identities { get; }

        public IUserRepository Users { get; }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}