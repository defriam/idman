using System.Collections.Generic;
using System.Threading.Tasks;
using IdMan.API.Data.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace IdMan.API.Data.Repository.User
{
    public class UserRepository : Repository<Models.User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }

        private IdManContext IdManContext => Context as IdManContext;

        public async Task<Models.User> GetUserAsync(int id)
        {
            return await IdManContext.Users.FindAsync(id);
        }

        public async Task<IEnumerable<Models.User>> GetUsersAsync()
        {
            return await IdManContext.Users.ToListAsync();
        }

        public async Task<Models.User> GetUserByUserNameAsync(string username)
        {
            return await IdManContext.Users.FirstOrDefaultAsync(x => x.UserName == username);
        }

        public Task AddUserAsync(Models.User user)
        {
            return IdManContext.Users.AddAsync(user);
        }
    }
}