using System.Collections.Generic;
using System.Threading.Tasks;

namespace IdMan.API.Data.Repository.User
{
    public interface IUserRepository
    {
        Task<IEnumerable<Models.User>> GetUsersAsync();
        Task<Models.User> GetUserAsync(int id);
        Task<Models.User> GetUserByUserNameAsync(string username);
        Task AddUserAsync(Models.User user);
    }
}