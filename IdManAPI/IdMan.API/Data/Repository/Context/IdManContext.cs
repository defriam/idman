using Microsoft.EntityFrameworkCore;

namespace IdMan.API.Data.Repository.Context
{
    public class IdManContext : DbContext
    {
        public IdManContext(DbContextOptions<IdManContext> options) : base(options)
        {
        }

        public DbSet<Models.User> Users { get; set; }
        public DbSet<Models.Identity> Identities { get; set; }
    }
}