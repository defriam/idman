using System.Collections.Generic;
using System.Threading.Tasks;

namespace IdMan.API.Data.Repository.Identity
{
    public interface IIdentityRepository : IRepository<Models.Identity>
    {
        Task<IEnumerable<Models.Identity>> GetIdentitiesAsync();
        Task<Models.Identity> GetIdentityAsync(int id);
    }
}