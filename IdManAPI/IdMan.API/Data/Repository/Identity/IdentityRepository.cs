using System.Collections.Generic;
using System.Threading.Tasks;
using IdMan.API.Data.Repository.Context;
using Microsoft.EntityFrameworkCore;

namespace IdMan.API.Data.Repository.Identity
{
    public class IdentityRepository : Repository<Models.Identity>, IIdentityRepository
    {
        public IdentityRepository(IdManContext context) : base(context)
        {
        }

        private IdManContext IdManContext => Context as IdManContext;

        public async Task<Models.Identity> GetIdentityAsync(int id)
        {
            return await IdManContext.Identities.FindAsync(id);
        }

        public async Task<IEnumerable<Models.Identity>> GetIdentitiesAsync()
        {
            return await IdManContext.Identities.ToListAsync();
        }
    }
}