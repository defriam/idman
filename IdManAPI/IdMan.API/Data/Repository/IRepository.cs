using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IdMan.API.Data.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void AddAsync(TEntity entity);
        void AddRangeAsync(IEnumerable<TEntity> entities);
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> GetAsync(int id);
        Task<IEnumerable<TEntity>> GetAllAsyc();
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
    }
}