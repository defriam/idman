using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using IdMan.API.Data.Repository.Context;
using IdMan.API.Models;
using Newtonsoft.Json;

namespace IdMan.API.Data.Seeder
{
    public static class Seed
    {
        public static void SeedIdNumbers(IdManContext context)
        {
            if (!context.Identities.Any())
            {
                var data = File.ReadAllText("Data/Seeder/SampleData/IdNumbersSeed.json");
                var identities = JsonConvert.DeserializeObject<List<Identity>>(data);
                foreach (var identity in identities)
                {
                    identity.DateOfBirth = DateTime.ParseExact(identity?.IdNumber.Substring(0, 6), "yyMMdd", null)
                        .ToString("yyMMdd");
                    identity.Citizenship = identity.Citizenship.ToLower();
                    identity.Gender = identity.Gender.ToLower();
                    identity.IdNumber = identity.IdNumber;

                    context.Add(identity);
                }

                context.SaveChanges();
            }
        }

        public static void SeedUsers(IdManContext context)
        {
            if (!context.Users.Any())
            {
                var data = File.ReadAllText("Data/Seeder/SampleData/UsersSeed.json");
                var users = JsonConvert.DeserializeObject<List<User>>(data);

                foreach (var user in users)
                {
                    CreatePasswordHash("password", out var passwordHash, out var passwordSalt);
                    user.PasswordHash = passwordHash;
                    user.PasswordSalt = passwordSalt;

                    user.UserName = user.UserName.ToLower();
                    context.Users.Add(user);
                }
            }

            context.SaveChanges();
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }

            ;
        }
    }
}