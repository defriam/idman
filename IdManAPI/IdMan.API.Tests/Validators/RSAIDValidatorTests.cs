using System;
using IdMan.API.Validators;
using NUnit.Framework;

namespace IdMan.API.Tests.Validators
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            Validator = new RSAIDValidator();
        }

        private RSAIDValidator Validator { get; set; }

        [Test]
        public void Validate_WhenCalledWithInvalidIdNumber_ShouldReturnFalse()
        {
            // Arrange

            // Act
            var result = Validator.Validate("1234567891111");

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void Validate_WhenCalledWithInvalidIdNumberLength_ShouldReturnFalse()
        {
            // Arrange

            // Act
            var result = Validator.Validate("123456789");

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetCitizenshipCitizen()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "Citizen";

            // Act
            var result = validator.Validate("8801234800087");

            // Assert
            Assert.True(
                string.Equals(validator.Citizenship, expectedResult, StringComparison.InvariantCultureIgnoreCase),
                $"our value {expectedResult}, actual {validator.Gender}");
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetCitizenshipResident()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "Resident";

            // Act
            var result = validator.Validate("8801234800186");

            // Assert
            Assert.True(
                string.Equals(validator.Citizenship, expectedResult, StringComparison.InvariantCultureIgnoreCase),
                $"our value {expectedResult}, actual {validator.Gender}");
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetDateOfBirth()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "880123";

            // Act
            var result = validator.Validate("8801235111088");

            // Assert
            Assert.AreEqual(validator.DateOfBirth, expectedResult);
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetGenderFemale()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "Female";

            // Act
            var result = validator.Validate("8801234800087");

            // Assert
            Assert.True(string.Equals(validator.Gender, expectedResult, StringComparison.InvariantCultureIgnoreCase),
                $"our value {expectedResult}, actual {validator.Gender}");
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetGenderMale()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "Male";

            // Act
            var result = validator.Validate("8801235111088");

            // Assert
            Assert.True(string.Equals(validator.Gender, expectedResult, StringComparison.InvariantCultureIgnoreCase));
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumber_ShouldSetIdNumber()
        {
            // Arrange
            var validator = new RSAIDValidator();
            var expectedResult = "8801235111088";

            // Act
            var result = validator.Validate("8801235111088");

            // Assert
            Assert.AreEqual(validator.IdNumber, expectedResult);
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumberWithoutWhiteSpaces_ShouldReturnTrue()
        {
            // Arrange

            // Act
            var result = Validator.Validate("8801235111088");

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void Validate_WhenCalledWithValidIdNumberWithWhiteSpaces_ShouldReturnTrue()
        {
            // Arrange

            // Act
            var result = Validator.Validate("8	8	0	1	2	3	5	1	1	1	0	8	8");

            // Assert
            Assert.IsTrue(result);
        }
    }
}