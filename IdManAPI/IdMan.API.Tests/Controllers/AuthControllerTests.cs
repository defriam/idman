using System.Threading.Tasks;
using IdMan.API.Controllers;
using IdMan.API.Dtos;
using IdMan.API.Services.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;

namespace IdMan.API.Tests.Controllers
{
    [TestFixture]
    public class AuthControllerTests
    {
        [Test]
        public void Register_WhenCalledWithExistingUser_ShouldReturnBadRequest()
        {
            var mockAuthService = new Mock<IAuthorizationService>();
            var mockConfiguration = new Mock<IConfiguration>();
            var controller = new AuthController(mockAuthService.Object, mockConfiguration.Object);

            var registerUser = new RegisterUserDto {Username = "foobared", Password = "foobared"};
            mockAuthService.Setup(m => m.UserExists(registerUser.Username)).Returns(Task.FromResult(true));

            var response = controller.Register(registerUser);

            Assert.IsInstanceOf<BadRequestObjectResult>(response.Result);
        }

        [Test]
        public void Register_WhenCalledWithNewUser_ShouldReturnStatusCodeResult201()
        {
            var mockAuthService = new Mock<IAuthorizationService>();
            var mockConfiguration = new Mock<IConfiguration>();
            var controller = new AuthController(mockAuthService.Object, mockConfiguration.Object);

            var registerUser = new RegisterUserDto {Username = "foo", Password = "foobared"};

            var response = controller.Register(registerUser);

            Assert.IsInstanceOf<StatusCodeResult>(response.Result);
        }
    }
}