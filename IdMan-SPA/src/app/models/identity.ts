export interface Identity {
    idNumber: number;
    dateOfBirth: string;
    gender: string;
    citizenship: string;
}
