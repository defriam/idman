import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Identity } from '../models/identity';

@Injectable({
  providedIn: 'root'
})
export class IdentityService {
  baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getIdentities(): Observable<Identity[]> {
    return this.http.get<Identity[]>(`${this.baseUrl}identities`);
  }

  getIdentity(id: any): Observable<Identity> {
    return this.http.get<Identity>(`${this.baseUrl}identities/${id}`);
  }

  validateIdentity(identity: any) {
    return this.http.put(`${this.baseUrl}identities/${identity.idNumber}`, identity);
  }
}
