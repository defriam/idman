import { TestBed } from '@angular/core/testing';
import { IdentityService } from './identity.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

const Identities = require('../../../src/assets/SampleData/IdNumbersSeed.json');
const baseUrl = environment.apiUrl;


const Identity = {
  IdNumber: '1234567891111',
  Username: 'Kim',
  DateOfBirth: '880123',
  Citizenship: 'Citizen',
  Gender: 'Female'
};

describe('IdentityService', () => {
  let identityService: IdentityService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [IdentityService]
    });

    (identityService = TestBed.get(IdentityService)),
      (httpTestingController = TestBed.get(HttpTestingController));
  });

  it('should retrieve all Identities', () => {
    identityService.getIdentities().subscribe(data => {
      expect(Identities.length).toBe(4, 'incorrect number of Identities');
    });

    const req = httpTestingController.expectOne(`${baseUrl}identities`);

    expect(req.request.method).toEqual('GET');

    req.flush({ payload: Object.values(Identities) });
  });

  it('should find a identity by id', () => {
    identityService.getIdentity(1234567891111).subscribe(identity => {
      expect(identity).toBeTruthy();
      expect(Identity.IdNumber).toBe('1234567891111');
    });

    const req = httpTestingController.expectOne(
      `${baseUrl}identities/1234567891111`
    );

    expect(req.request.method).toEqual('GET');

    req.flush(Identity);
  });

  it('should save the identity number', () => {

    const identityToStore = {
      idNumber: '1234567891111',
      username: 'Kim',
      dateOfBirth: '880123',
      citizenship: 'Citizen',
      gender: 'Female'
    };

    identityService.validateIdentity(identityToStore).subscribe(identity => {
      expect(identity).toBeTruthy();
      expect(Identity.IdNumber).toBe('1234567891111');
    });

    const req = httpTestingController.expectOne(
      `${baseUrl}identities/1234567891111`
    );

    expect(req.request.method).toEqual('PUT');

    req.flush(identityToStore);
  });
});
