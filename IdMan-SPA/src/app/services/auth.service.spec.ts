/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

const baseUrl = environment.apiUrl;

describe('AuthService', () => {

  let authService: AuthService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService]
    });
    (authService = TestBed.get(AuthService)),
      (httpTestingController = TestBed.get(HttpTestingController));
  });


  const User = {
    username: 'abcd',
    password: 'password'
  };

  it('should create', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));

  it('should login user', () => {
    authService.login(User).subscribe(response => {
      expect(response).toBe(null, 'User failed to login');
    });

    const req = httpTestingController.expectOne(`${baseUrl}auth/login`);

    expect(req.request.method).toEqual('POST');

  });
});
