import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AlertifyService } from '../services/alertify.service';
import { IdentityService } from '../services/identity.service';
import { Identity } from '../models/identity';
import { AuthService } from '../services/auth.service';
import {
  FormGroup,
  Validators,
  FormBuilder
} from '@angular/forms';

@Component({
  selector: 'app-identity',
  templateUrl: './identity.component.html',
  styleUrls: ['./identity.component.css']
})
export class IdentityComponent implements OnInit {
  @Output() validatedId = new EventEmitter<Identity>();
  identities: Identity[];
  identity: Identity;
  identityForm: FormGroup;

  constructor(
    private identityService: IdentityService,
    private alertify: AlertifyService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.createIdentityForm();
    this.loadIdentities();
  }

  createIdentityForm() {
    this.identityForm = this.formBuilder.group(
      {
        idNumber: [
          null,
          [
            Validators.required,
            Validators.minLength(13),
            Validators.maxLength(13)
          ]
        ]
      },
      { validator: this.isNumberValidator }
    );
  }

  isNumberValidator(g: FormGroup) {
    let isnumber = /^\d+$/.test(g.get('idNumber').value)
      ? null
      : { notanumber: true };
    return isnumber;
  }

  loadIdentities() {
    this.identityService.getIdentities().subscribe(
      (identities: Identity[]) => {
        this.identities = identities;
      },
      error => {
        this.alertify.error(error);
      }
    );
  }

  validateIdentity() {
    if (this.identityForm.valid) {
      this.identity = Object.assign({}, this.identityForm.value);
      this.validatedId.emit(this.identity);
      this.identityService.validateIdentity(this.identity).subscribe(() => {
        this.alertify.success('ID number validation success');
      }, error => {
        this.alertify.error(`ID number validation failed. \n${error}`);
      }, () => this.loadIdentities());
    }
  }

  refreshIdentities() {
    this.loadIdentities();
  }
}
